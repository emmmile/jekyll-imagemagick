require 'minitest/autorun'
require 'test/unit'
require 'jekyll-imagemagick/defaults'
require 'jekyll-imagemagick/convert'
require 'jekyll'

Jekyll.logger.log_level = :error

# This is more like an integration test to see that commands are passed
# to the convert binary.

class ImagemagickTest < Test::Unit::TestCase
  include JekyllImagemagick

  def test_imagemagick_can_convert_webp
    _output, error = ImageConvert.run('test/black.png', 'test/black.webp', '', 0, '')
    assert_equal(nil, error)
  end

  # This test is broken on MACOS 10.13, Homebrew, Imagemagick 7.0.8-10
  def imagemagick_can_convert_to_webp_with_quality_flag
    # flags = '-quality 95% -define webp:pass=10 -define webp:method=6 -define webp:low-memory=true'
    flags = '-quality 95%'
    _output, error = ImageConvert.run('test/black.png', 'test/black.webp', flags, 0, '')
    assert_equal(nil, error)

    ImageConvert.run('test/black.png', 'test/100.webp', '-quality 100%', 0, '')
    ImageConvert.run('test/black.png', 'test/10.webp', '-quality 10%', 0, '')
    assert_not_equal(File.size('test/100.webp'), File.size('test/10.webp'))
  end
end
