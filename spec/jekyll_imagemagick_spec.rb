# frozen_string_literal: true

require 'spec_helper'

describe(JekyllImagemagick) do
  let(:overrides) { {} }
  let(:config) do
    Jekyll.configuration(Jekyll::Utils.deep_merge_hashes({
      'full_rebuild' => true,
      'source'       => source_dir,
      'destination'  => dest_dir,
      'show_drafts'  => true,
      'url'          => 'http://example.org',
      'name'         => 'My awesome site',
      'author'       => {
        'name' => 'Dr. Jekyll'
      },
      'collections' => {
        'my_collection' => { 'output' => true },
        'other_things'  => { 'output' => false }
      }
    }, overrides))
  end
  let(:site)     { Jekyll::Site.new(config) }
  let(:context)  { make_context(site => site) }
  before(:each) do
    site.cleanup
    site.process
  end

  it 'rspec works' do
    expect(0).to match(0)
  end
end
